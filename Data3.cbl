       IDENTIFICATION DIVISION.
       PROGRAM-ID. DATA3.
       AUTHOR. Naranya.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  SURNAME     PIC X(8) VALUE 'COUGHLAN'.
       01  SALE-PRICE  PIC 9(4)V99.
       01  NUM-OF-EMPLOYEE   PIC   999V99.
       01  SALARY      PIC 9999V99.
       01  COUNTY-NAME PIC X(99).
       PROCEDURE DIVISION .
       Begin.
           DISPLAY  "1 "     SURNAME
           MOVE     "SMITH"  TO SURNAME
           DISPLAY  "2 "     SURNAME
           MOVE     "FITZWILLIAM"   TO SURNAME
           DISPLAY  "3 "     SURNAME
           DISPLAY  "1 "     SALE-PRICE
           MOVE     ZERO     TO   SALE-PRICE
           DISPLAY  "2 "     SALE-PRICE
           MOVE     25.5     TO    SALE-PRICE
           DISPLAY  "3 "     SALE-PRICE
      *    01    SALE-PRICE     PIC   9(4)V99.    0007.55
           MOVE     7.553    TO    SALE-PRICE
           DISPLAY  "4 "     SALE-PRICE
      *    01    SALE-PRICE     PIC   9(4)V99.    3425.15
           MOVE     93425.158    TO    SALE-PRICE
           DISPLAY  "5 "     SALE-PRICE
      *    01    SALE-PRICE     PIC   9(4)V99.    0128.00
           MOVE     128      TO    SALE-PRICE
           DISPLAY  "6 "     SALE-PRICE
           DISPLAY "7 "      NUM-OF-EMPLOYEE
      *    01  NUM-OF-EMPLOYEE   PIC   999V99.    012.40
           MOVE     12.4     TO    NUM-OF-EMPLOYEE
           DISPLAY "8 "      NUM-OF-EMPLOYEE
      *    01  NUM-OF-EMPLOYEE   PIC   999V99.    745.00
           MOVE     6745     TO    NUM-OF-EMPLOYEE
           DISPLAY "9 "      NUM-OF-EMPLOYEE
      *    01  SALARY      PIC 9999V99.            0745.00
           MOVE NUM-OF-EMPLOYEE TO    SALARY
           DISPLAY "10 " SALARY
      *    01  COUNTY-NAME PIC X(99).
           MOVE "GALWAY" TO COUNTY-NAME
           DISPLAY "11 " COUNTY-NAME
           MOVE ALL "*-"   TO COUNTY-NAME
           DISPLAY "12 " COUNTY-NAME
           .

      